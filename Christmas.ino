#include <LiquidCrystal.h>

const int 
  rs = 2, 
  en = 3, 
  d4 = 4, 
  d5 = 5, 
  d6 = 6, 
  d7 = 7;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

int length = 26;
char notes[] = "eeeeeeegcde fffffeeeeddedg";
int beats[] = { 1, 1, 2, 1, 1, 2, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2};

int tempo = 300;

int LED1  = 12;
int LED2  = 11;
int LED3  = 10;
int SPEAKER = 8;
int BEATTIME = 100;


int zaehler = 0;
int maxLength = 17;
const int StringLength = 55;
char nachricht[StringLength] = "Ich schenke dir einen Gutschein fuer deinen Laptop :-)";


void setup() {
  lcd.begin(16, 2);
  lcd.print("Frohe Weihnacht");

  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(SPEAKER, OUTPUT);
}


void loop() {
  for (int i = 0; i < length; i++) {
    if (notes[i] == ' ') {
      delay(beats[i] * tempo); // rest
    } else {
      playNote(notes[i], beats[i] * tempo);
    }
    
    TurnOnLed(LED1);
    TurnOnLed(LED2);
    TurnOnLed(LED3);
  
    // pause between notes
    delay(tempo / 2); 

    
    lcd.setCursor(0, 1);
    if ((zaehler + maxLength) > StringLength) {
      zaehler = 0;
    }
    else {
    

//    if (zaehler == 0)
//      delay (1000);
    
    for (int i = zaehler; i<zaehler + maxLength; i++){
      lcd.print(nachricht[i]);
    }
    
    zaehler = zaehler + 1;
//    delay (500);
    }
  
  }  
  
}


void playNote(char note, int duration) {
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
  int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };
  
  // play the tone corresponding to the note name
  for (int i = 0; i < 8; i++) {
    if (names[i] == note) {
      playTone(tones[i], duration);
    }
  }
}

void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(SPEAKER, HIGH);
    delayMicroseconds(tone);
    digitalWrite(SPEAKER, LOW);
    delayMicroseconds(tone);
  }
}

void TurnOnLed(int pin){
  digitalWrite(pin, LOW);
  
  int r = random(0,2);
  if (r == 1) {
    digitalWrite(pin, HIGH);
  }
}


void TurnOffLed(int pin){
  if (random(0,1) == 0) {
    digitalWrite(pin, LOW);
  }
}
